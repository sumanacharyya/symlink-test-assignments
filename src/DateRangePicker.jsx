import React, { useEffect, useState } from "react";
import { DatePicker } from "antd";
import dayjs from "dayjs";
import moment from "moment";

const { RangePicker } = DatePicker;

const DateRangePicker = ({
  Form,
  startDate,
  setStartDate,
  endDate,
  setEndDate,
  dateFormat,
}) => {
  // useEffect(() => {
  //   console.log(new Date(startDate).toLocaleDateString("en-CA"), endDate);
  // }, [startDate, endDate]);

  return (
    <div>
      <Form.Item
        label="Duration"
        // onChange={(e) => {
        //   console.log(e);
        // }}
      >
        <RangePicker
          onChange={(e) => {
            console.log(e);
          }}
          // value={[moment(startDate), moment(endDate)]}
          defaultValue={[moment(startDate), moment(endDate)]}
          format={dateFormat}
        />
      </Form.Item>
    </div>
  );
};

export default DateRangePicker;
