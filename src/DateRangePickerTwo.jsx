import React, { useEffect, useState } from "react";

// DateTimeRangePicker
import DateTimeRangePicker from "@wojtekmaj/react-datetimerange-picker";
import "@wojtekmaj/react-datetimerange-picker/dist/DateTimeRangePicker.css";
import "react-calendar/dist/Calendar.css";
import "react-clock/dist/Clock.css";

// DateRangePicker
import DateRangePicker from "@wojtekmaj/react-daterange-picker";
import "@wojtekmaj/react-daterange-picker/dist/DateRangePicker.css";
import "react-calendar/dist/Calendar.css";
import InputGroupAnt from "./InputGroupAnt";

const DateRangePickerTwo = () => {
  const [value, onChange] = useState([new Date(), new Date()]);
  const [valueDate, onChangeDate] = useState([new Date(), new Date()]);
  const [inputState, setInputState] = useState(["", ""]);

  const onChangeVal = (e) => {
    console.log(e);
    onChange(value);
  };

  const onChangeDateVal = (e) => {
    console.log(e);
    onChangeDate(e);
  };

  useEffect(() => {
    setTimeout(() => {
      const fetcedTime = [new Date("2023-06-11"), new Date("2023-06-17")];
      const fetcedTnput = ["32.4535436", "43.4457645"];
      onChangeDate(fetcedTime);
      setInputState(fetcedTnput);
    }, 1500);
  }, []);

  console.log(inputState);

  return (
    <div>
      {/* DateTimeRangePicker */}
      {/* <DateTimeRangePicker
        minDate={new Date(Date.now())}
        onChange={onChangeVal}
        value={value}
      /> */}
      <br />

      {/* DateRangePicker */}
      <DateRangePicker
        minDate={new Date(Date.now())}
        onChange={onChangeDateVal}
        value={valueDate}
        format="y-MMM-dd"
      />

      <InputGroupAnt inputState={inputState} setInputState={setInputState} />
    </div>
  );
};
export default DateRangePickerTwo;
