import React from "react";
import { Form, Input, Space } from "antd";
import { AimOutlined } from "@ant-design/icons";

const InputGroupAnt = ({ inputState, setInputState }) => {
  console.log(inputState);

  const onChangeValueSetterandler = (name, value) => {
    const newInputState = [...inputState];
    if (name === "latitude") {
      newInputState[0] = value;
    }
    if (name === "longitude") {
      newInputState[1] = value;
    }
    setInputState([...newInputState]);
  };

  return (
    <Space direction="vertical" size="middle">
      <Form.Item label="Location">
        <Input
          value={inputState[0]}
          addonBefore={<AimOutlined />}
          placeholder="Latitude"
          onChange={(e) =>
            onChangeValueSetterandler("latitude", e.target.value)
          }
        />
        <Input
          value={inputState[1]}
          placeholder="Longitude"
          onChange={(e) =>
            onChangeValueSetterandler("longitude", e.target.value)
          }
        />
      </Form.Item>
    </Space>
  );
};

export default InputGroupAnt;
