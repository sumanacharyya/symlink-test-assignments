import { useEffect, useState } from "react";
import DateRangePicker from "./DateRangePicker";
import { Form } from "antd";
import dayjs from "dayjs";
import moment from "moment/moment";
import DateRangePickerTwo from "./DateRangePickerTwo";

function App() {
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  const dateFormat = "YYYY/MM/DD";

  const handleSubmit = () => {
    console.log(Form.getFieldsValue());
  };

  useEffect(() => {
    setTimeout(() => {
      setStartDate("2015/01/01");
      setEndDate("2015/01/01");
      // setStartDate(moment("2015/01/01", dateFormat)?._d);
      // setEndDate(moment("2015/01/01", dateFormat)?._d);
    }, 1500);
  }, []);

  return (
    <>
      {/* <Form onFinish={handleSubmit}>
        <DateRangePicker
          Form={Form}
          startDate={startDate}
          setStartDate={setStartDate}
          endDate={endDate}
          setEndDate={setEndDate}
          dateFormat={dateFormat}
        />
      </Form> */}
      <DateRangePickerTwo />
    </>
  );
}

export default App;
